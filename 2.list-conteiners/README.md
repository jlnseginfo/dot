Criada uma imagem com o comando docker.
Essa imagem, ao ser executada, monta a partição /var/run do host, que é
utilizada pelo docker ao ser executado.
A execução do comando é "docker ps", que lista todos os containers que estão
sendo executados no momento

sudo docker build -t test .
sudo docker run -it --mount type=bind,source=/var/run,target=/var/run test
